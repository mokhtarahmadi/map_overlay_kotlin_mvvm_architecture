package com.ahmadi.mokhtar.kotlin_mvvm_architecture.viewModels

import androidx.lifecycle.ViewModel
import com.ahmadi.mokhtar.kotlin_mvvm_architecture.api.Api
import com.ahmadi.mokhtar.kotlin_mvvm_architecture.di.DIComponent
import com.ahmadi.mokhtar.kotlin_mvvm_architecture.models.LocationModel
import com.ahmadi.mokhtar.kotlin_mvvm_architecture.utils.NavigationEvent
import com.google.android.gms.maps.model.LatLng
import io.reactivex.Single
import io.reactivex.processors.PublishProcessor
import javax.inject.Inject

class TripDataFragmentViewModel: ViewModel(), DIComponent.Injectable {
    override fun inject(diComponent: DIComponent) {
        diComponent.inject(this)
    }

    @Inject
    lateinit var navEvents: PublishProcessor<NavigationEvent>

    @Inject
    lateinit var api: Api

    fun openNavigation() {
        navEvents.onNext(NavigationEvent(NavigationEvent.NavEvent.OPEN_DRAWER))
    }


    fun getCurrentLocationData(loc: LatLng): Single<LocationModel> =
        api.getCurrentLocationData("jsonv2", loc.latitude, loc.longitude)
}