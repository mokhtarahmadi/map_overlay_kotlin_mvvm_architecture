package com.ahmadi.mokhtar.kotlin_mvvm_architecture.di

import com.ahmadi.mokhtar.kotlin_mvvm_architecture.App
import com.ahmadi.mokhtar.kotlin_mvvm_architecture.utils.NavigationEvent
import dagger.Module
import dagger.Provides
import io.reactivex.processors.PublishProcessor
import javax.inject.Singleton

@Module
class AppModule(private val app: App) {

    @Provides
    fun providesApp(): App = app

    @Provides
    @Singleton
    fun providesNavEventProcessor(): PublishProcessor<NavigationEvent> = PublishProcessor.create()
}