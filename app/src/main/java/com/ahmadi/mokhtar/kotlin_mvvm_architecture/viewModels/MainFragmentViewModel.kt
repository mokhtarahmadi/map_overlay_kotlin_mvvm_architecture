package com.ahmadi.mokhtar.kotlin_mvvm_architecture.viewModels

import androidx.lifecycle.ViewModel
import com.ahmadi.mokhtar.kotlin_mvvm_architecture.di.DIComponent
import com.ahmadi.mokhtar.kotlin_mvvm_architecture.di.DIComponent.Injectable
import com.ahmadi.mokhtar.kotlin_mvvm_architecture.utils.NavigationEvent
import io.reactivex.processors.PublishProcessor
import javax.inject.Inject

class MainFragmentViewModel : ViewModel() , Injectable {
    override fun inject(diComponent: DIComponent) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

        diComponent.inject(this)
    }

    @Inject
    lateinit var navEvents : PublishProcessor<NavigationEvent>

    fun openNavigation(){
        navEvents.onNext(NavigationEvent(NavigationEvent.NavEvent.OPEN_DRAWER))
    }
}