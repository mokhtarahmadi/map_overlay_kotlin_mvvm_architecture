package com.ahmadi.mokhtar.kotlin_mvvm_architecture

class Constants {
    companion object {
        const val BASE_URL = "https://nominatim.openstreetmap.org/"
        const val FA_LANG = "fa"
        const val EN_LANG = "en"
    }
}