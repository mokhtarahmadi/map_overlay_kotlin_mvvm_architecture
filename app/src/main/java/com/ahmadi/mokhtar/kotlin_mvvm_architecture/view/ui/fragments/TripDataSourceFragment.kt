package com.ahmadi.mokhtar.kotlin_mvvm_architecture.view.ui.fragments

import android.view.View
import androidx.lifecycle.ViewModelProviders
import com.ahmadi.mokhtar.kotlin_mvvm_architecture.App
import com.ahmadi.mokhtar.kotlin_mvvm_architecture.R
import com.ahmadi.mokhtar.kotlin_mvvm_architecture.models.LocationModel
import com.ahmadi.mokhtar.kotlin_mvvm_architecture.utils.*
import com.ahmadi.mokhtar.kotlin_mvvm_architecture.view.ui.base.BaseFragment
import com.ahmadi.mokhtar.kotlin_mvvm_architecture.viewModels.TripDataFragmentViewModel
import com.ahmadi.mokhtar.kotlin_mvvm_architecture.viewModels.ViewModelFactory
import com.google.android.gms.maps.model.LatLng
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.processors.PublishProcessor
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.fragment_trip_data_source.*
import timber.log.Timber

class TripDataSourceFragment :BaseFragment(), View.OnClickListener {
    override fun onClick(v: View?) {
        when(v){
            tvAccept ->{
                changeUiToDest()
            }
        }
    }

    private val viewDestroyCompositeDisposable = CompositeDisposable()
    private val destroyCompositeDisposable = CompositeDisposable()

    private lateinit var viewModel: TripDataFragmentViewModel
    private lateinit var currentAddress: LocationModel


    private fun changeUiToDest() {
        mainFragment.mainActionsPublisher.onNext(Pair(mainFragment.ActionType.ACCEPT_SOURCE , currentAddress))
    }

    companion object {
        var tripDataAcceptPublisher : PublishProcessor<Pair<mainFragment.ActionType , Any?>> = PublishProcessor.create()

        fun newInstance() = TripDataSourceFragment()
    }
    override fun initBeforeView() {
        with(context!!.applicationContext as App){
            viewModel = ViewModelProviders.of(this@TripDataSourceFragment ,
                ViewModelFactory(this)).get(TripDataFragmentViewModel::class.java)

        }
    }

    override fun getContentViewId(): Int  = R.layout.fragment_trip_data_source

    override fun initViews(rootView: View) {
        tvAccept.setOnClickListener(this)
        tvAccept.changeState(ViewType.DISABLE, R.drawable.rectangle_shape_fill_dark)

        tripDataAcceptPublisher.observeOn(AndroidSchedulers.mainThread()).
                subscribe{
                    when(it.first){
                        mainFragment.ActionType.SHOW_LOCATION_DATA ->{
                            getCurrentLocationData(it.second as LatLng)
                        }
                    }
                }.addTo(viewDestroyCompositeDisposable)

    }

    private fun getCurrentLocationData(latLng: LatLng) {
        viewModel.getCurrentLocationData(latLng)
            .iomain()
            .doOnSubscribe {
                tvAccept.changeState(ViewType.DISABLE, R.drawable.rectangle_shape_fill_dark)
                tvOriginTitle.text = getString(R.string.loading_address)
                tvOriginSubTitle.gone()
            }.subscribe({
                currentAddress = it
                tvOriginTitle.text = it.getMainAddress()
                tvOriginSubTitle.text = it.getSubAddress()

                tvAccept.changeState(ViewType.ENABLE, R.drawable.rectangle_shape_fill_blue)
                tvOriginSubTitle.visible()
            }, {
                Timber.e(it)
                CustomToast.makeText(context!!, getString(R.string.server_error), CustomToast.ERROR)
            }).addTo(viewDestroyCompositeDisposable)
    }

    override fun onDestroy() {
        destroyCompositeDisposable.clear()
        super.onDestroy()
    }

    override fun onDestroyView() {
        viewDestroyCompositeDisposable.clear()
        super.onDestroyView()
    }
}