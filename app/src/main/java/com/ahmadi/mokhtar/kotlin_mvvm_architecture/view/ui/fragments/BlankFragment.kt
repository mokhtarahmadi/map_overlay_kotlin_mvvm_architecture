package com.ahmadi.mokhtar.kotlin_mvvm_architecture.view.ui.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ahmadi.mokhtar.kotlin_mvvm_architecture.R
import com.ahmadi.mokhtar.kotlin_mvvm_architecture.utils.AppBarType
import com.ahmadi.mokhtar.kotlin_mvvm_architecture.utils.initToolbar
import com.ahmadi.mokhtar.kotlin_mvvm_architecture.view.ui.activities.MainActivity
import com.ahmadi.mokhtar.kotlin_mvvm_architecture.view.ui.base.BaseFragment
import kotlinx.android.synthetic.main.app_bar_main.view.*


class BlankFragment : BaseFragment() {
    override fun initBeforeView() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getContentViewId(): Int  = R.layout.fragment_blank

    override fun initViews(rootView: View) {
        setHasOptionsMenu(true)
        rootView.toolbarMain.initToolbar(activity as MainActivity,
            getString(R.string.blank_fragment_title), AppBarType.BACK)
    }




}
