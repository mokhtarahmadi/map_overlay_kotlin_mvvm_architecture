package com.ahmadi.mokhtar.kotlin_mvvm_architecture.utils

class NavigationEvent(val navEvent : NavEvent , vararg val payloads: Any) {

    enum class NavEvent {
        GO_BACK,
        OPEN_DRAWER
    }
}