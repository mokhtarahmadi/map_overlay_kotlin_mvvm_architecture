package com.ahmadi.mokhtar.kotlin_mvvm_architecture.di


import androidx.annotation.Keep
import com.ahmadi.mokhtar.kotlin_mvvm_architecture.view.ui.activities.MainActivity
import com.ahmadi.mokhtar.kotlin_mvvm_architecture.viewModels.MainFragmentViewModel
import com.ahmadi.mokhtar.kotlin_mvvm_architecture.viewModels.TripDataFragmentViewModel
import dagger.Component
import javax.inject.Singleton

@Keep
@Singleton
@Component(modules = [AppModule::class, ApiModule::class])
interface DIComponent {

    interface Injectable {
        fun inject(diComponent: DIComponent)
    }

    fun inject(mainFragmentViewModel: MainFragmentViewModel)
    fun inject(mainActivity: MainActivity)
    fun inject(tripDataFragmentViewModel: TripDataFragmentViewModel)
}