package com.ahmadi.mokhtar.kotlin_mvvm_architecture.api

import com.ahmadi.mokhtar.kotlin_mvvm_architecture.models.LocationModel
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {
    @GET("reverse")
    fun getCurrentLocationData(@Query("format") format: String,
                               @Query("lat") lat: Double,
                               @Query("lon") lon: Double): Single<LocationModel>
}