package com.ahmadi.mokhtar.kotlin_mvvm_architecture.utils

interface PermissionCallback {
    fun onGranted(permission:Array<String>)
    fun onDenied(permission: Array<String>)
    fun onShowRationale(permission: Array<String>)
}