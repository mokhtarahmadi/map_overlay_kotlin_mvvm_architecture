package com.ahmadi.mokhtar.kotlin_mvvm_architecture.view.ui.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.ahmadi.mokhtar.kotlin_mvvm_architecture.R
import com.ahmadi.mokhtar.kotlin_mvvm_architecture.view.ui.base.BaseFragment
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import java.util.concurrent.TimeUnit


class splashFragment : BaseFragment() {
    private val destroyCompositeDisposable = CompositeDisposable()
    override fun initBeforeView() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getContentViewId(): Int = R.layout.fragment_splash

    override fun initViews(rootView: View) {
        Completable.timer(2, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
            .subscribe({
                Navigation.findNavController(view!!).navigate(R.id.action_splashFragment_to_mainFragment)
            }, {}).addTo(destroyCompositeDisposable)
    }


    override fun onDestroyView() {
        destroyCompositeDisposable.dispose()
        super.onDestroyView()
    }


}
